# Notas de estudo

![moça estudando no pc](img/womantech.png) ![caderno de notas](img/notebook.png) ![chapéu graduação](img/graduation_cap.png)

---

## Autômatos Finitos

![robô](img/robot.png)

0. [O que é um autômato](automatos_finitos/p0000_que_e_automato_finito.md)

1. [Definição formal de um autômato finito](automatos_finitos/p0001_definicao.md)


---

## Bases numéricas

![código binário](img/binary_code.png) ![ábaco](img/abaco.png)

0. [Decimal](bases_numericas/p0000_base_numerica_decimal.md)

1. [Binário](bases_numericas/p0001_base_numerica_bin.md)

2. [Octal](bases_numericas/p0002_base_numerica_oct.md)

3. [Hexadecimal](bases_numericas/p0003_base_numerica_hex.md)


---

## Linux

![pc com linux](img/pinguim_pc.png) ![linux mint](img/linux-mint.png) ![pinguim](img/pinguim.png)

0. [Terminal: Instalando programas](linux/p0000_install.md)

1. [Terminal: Desinstalando programas no Linux](linux/p0001_remove.md)

2. [Terminal: Limpando a tela](linux/p0002_clear.md)

3. [Terminal: Listando arquivos e diretórios no Linux](linux/p0003_ls.md)

4. [Terminal: Navegando nos diretórios Linux](linux/p0004_cd.md)

5. [Terminal: Atualizando o sistema](linux/p0005_update.md)

6. [Terminal: Removendo programas obsoletos](linux/p0006_autoremove.md)

7. [Terminal: Limpando ao cache do APT](linux/p0007_autoclean.md)

8. [Terminal: Corrigindo pacotes quebrados](linux/p0008_pacotes_quebrados.md)

9. [Terminal: Criando diretório no Linux](linux/p0009_mkdir.md)

10. [Terminal: Removendo arquivos e diretórios no Linux](linux/p0010_rmdir.md)

11. [Terminal: O comando cat](linux/p0011_cat.md)

12. [Terminal: Comando touch](linux/p0012_touch.md)

13. [Terminal: Verificar informações da CPU](linux/p0013_lscpu.md)

14. [Terminal: Verificar uso de espaço em disco](linux/p0014_df.md)

15. [Terminal: Desligando o computador](linux/p0015_shutdown.md)

16. [Terminal: Histórico de comandos](linux/p0016_historico.md)

17. [Instalando o Python 3.8 e o IDLE no Linux](linux/p0017_instalando_python3.8.md)

18. [Terminal: Buscando arquivo/diretórios no Linux](linux/p0018_busca.md)

19. [Instalando o VS Code pelo terminal](linux/p0019_vscode.md)

20. [Terminal: copiando arquivos e diretórios](linux/p0020_cp.md)

21. [Terminal: Informações sobre o usuário](linux/p0021_whoami.md)

22. [Terminal: movendo/renomeando arquivos e diretórios](linux/p0022_mv.md)

23. [Terminal: Verificando informações do sistema](linux/p0023_uname.md)

24. [Terminal: Verificando há quanto tempo o sistema está ligado](linux/p0024_uptime.md)

25. [Terminal: Adicionando usuário](linux/p0025_adduser.md)

26. [Terminal: Excluindo usuário](linux/p0026_userdel.md)

27. [Terminal: Mostrando o início ou fim de um arquivo texto](linux/p0027_head_tail.md)

28. [Terminal: Gerenciando grupos](linux/p0028_groups.md)

29. [Terminal: Contando palavras em um arquivo](linux/p0029_wc.md)

30. [Terminal: Visualizando processos](linux/p0030_top_htop.md)

31. [Terminal: O sistema de permissões no Linux](linux/p0031_permissoes.md)

32. [Terminal: Entendendo o sudo e o root](linux/p0032_root.md)

33. [Instalando o Node.JS e o Node Version Manager (NVM) no Linux (Ubuntu e derivados)](linux/p0033_nodeJS.md)

34. [Instalação e configuração LAMP (Linux, Apache, MariaDB e PHP) no Linux (Ubuntu e derivados)](linux/p0034_lamp.md)

35. [Instalação do Joomla-3.x](linux/p0035_joomla.md)


---

# Lógica

![and](img/and.png) ![not](img/not.png) ![or](img/or.png)

0. [Operadores lógicos e tabela verdade](logica/p0000_tabela_verd_e_ou_not.md)

1. [Tabela verdade: condicional](logica/p0001_tab_verd_condicional.md)

2. [Tabela verdade: bicondicional](logica/p0002_tab_verd_bicondicional.md)

3. [Tabela verdade: disjunção exclusiva](logica/p0003_disj_exclusiva.md)


---

## VS Code

0. [Erro ao usar a extensão Python Preview do VSCode](vscode/p0000_python_preview_problem.md)


---

## Windows

![laptop](img/laptop.png) ![windows os](img/windows_os.png)

0. [O Windows não pode ser instalado em partições GPT](windows/p0000_format_particao_gpt.md)
