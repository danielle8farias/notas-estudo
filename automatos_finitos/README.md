## Autômatos Finitos

![robô](../img/robot.png)


0. [O que é um autômato](p0000_que_e_automato_finito.md)

1. [Definição formal de um autômato finito](p0001_definicao.md)
